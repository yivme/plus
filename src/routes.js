import Search from './components/search/Search.jsx';
// import List from './components/list/List.jsx';
import AsyncList from './components/list/AsyncList.jsx';

const routes = [
    {
        exact: true,
        path: '/',
        component: Search
    },
    {
        path: '/user/:name',
        component: AsyncList
    }
    // {
    //     path: '/user/:name',
    //     component: List
    // }
];

export default routes;