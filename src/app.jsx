import React from 'react';
import { Switch } from 'react-router';
import { Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import routes from './routes';
import SingletonHistory from './utils/SingletonHistory';

export default function App() {
    const history = SingletonHistory();

    return <ConnectedRouter history={history}>
            <Switch>
            {routes.map((route, key) => {
                return <Route key={`sr_${key}`} {...route}/>;
            })}
        </Switch>
    </ConnectedRouter>
}