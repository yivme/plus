
export const DIRECTION = {
    ASC: 'asc',
    DESC: 'desc'
};

export const ITEM_STEP = 5;