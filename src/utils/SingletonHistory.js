import createHistory from 'history/createBrowserHistory';

export default function SingletonHistory () {
    if (SingletonHistory.instance) {
        return SingletonHistory.instance
    }
    SingletonHistory.instance = createHistory();

    return SingletonHistory.instance;
}