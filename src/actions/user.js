import { GET_USER_INFO } from '../constants/User';
import { DIRECTION, ITEM_STEP } from '../utils/constants';

export const getUserInfo = (userName, direction = DIRECTION.ASC, rowCount = ITEM_STEP) => {
    return dispatch => {
        fetch(`https://api.github.com/users/${userName}/repos?page=1&per_page=${rowCount}&sort=created&direction=${direction}`)
            .then(function(response) {
                return response.json();
            })
            .then(function(data) {
                return dispatch({
                    type: GET_USER_INFO,
                    payload: { list: data, direction, rowCount }
                });
            })
            .catch(function() {
                console.error('request error');
            });
    }
};