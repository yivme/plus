import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from '../reducers/index';
import { routerMiddleware } from 'react-router-redux';
import SingletonHistory from '../utils/SingletonHistory';

// Create a history of your choosing (we're using a browser history in this case)
const history = SingletonHistory();

// Build the middleware for intercepting and dispatching navigation actions
const histMiddleware = routerMiddleware(history);

const middleware = [thunk, histMiddleware];

export default function configureStore() {
    const store = createStore(
        rootReducer,
        applyMiddleware(...middleware)
    );

    return store
}


