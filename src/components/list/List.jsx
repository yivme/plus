import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUserInfo } from '../../actions/user';
import { DIRECTION, ITEM_STEP } from '../../utils/constants'

import './style.css';

class ListPure extends Component {

    handleClick = e => {
        e.preventDefault();
        const { direction, rowCount } = this.props;
        const reverse = direction === DIRECTION.ASC ? DIRECTION.DESC : DIRECTION.ASC;

        this.applyFilter(reverse, rowCount);
    };

    handleClickShowMore = e => {
        e.preventDefault();
        const { direction, rowCount } = this.props;

        this.applyFilter(direction, rowCount + ITEM_STEP);
    };

    applyFilter(direction, rowCount) {
        const { match: { params: { name }}, getUserInfo } = this.props;

        getUserInfo(name, direction, rowCount);
    }

    render() {
        const { userRepoList, direction } = this.props;

        return <div className="ui-list">
            <div className="ui-list__bar">
                <div>List:</div>
                <div>
                    <button onClick={this.handleClick}>
                        {direction === DIRECTION.ASC ? '↑' : '↓'}
                    </button>
                </div>
            </div>
            <div className="ui-list__table">
                {userRepoList.map(item => <div
                    className="ui-list__row"
                    key={item.id}
                >
                    <div className="ui-list__cell">{item.name}</div>
                    <div className="ui-list__cell">
                        {(new Date(item.created_at)).toLocaleString('ru')}
                    </div>
                </div>)}
            </div>
            <div className="ui-list__show-more">
                <button onClick={this.handleClickShowMore}>
                    Показать еще
                </button>
            </div>
        </div>
    }
}

const mapStateToProps = (state) => {
    return {
        userRepoList: state.user.userRepoList,
        direction: state.user.direction,
        rowCount: state.user.rowCount
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getUserInfo: (userName, direction, rowCount) => {
            dispatch(getUserInfo(userName, direction, rowCount))
        }
    }
};

const List = connect(
    mapStateToProps,
    mapDispatchToProps
)(ListPure);

export default List;