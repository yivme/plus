import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUserInfo } from '../../actions/user';

class AsyncListPure extends Component {

    state = { Component: null };

    componentWillMount() {
        const _this = this;
        const userName = this.props.match.params.name;

        this.props.getUserInfo(userName);

        require.ensure([], function() {
            const List = require('./List.jsx');

            _this.setComponent(List.default);
        });
    }

    setComponent = Component => {
        this.setState({
            Component
        });
    };

    render() {
        const { Component } = this.state;

        return !Component ? <div>loading...</div> : <Component {...this.props} />;
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getUserInfo: (userName, direction, rowCount) => {
            dispatch(getUserInfo(userName, direction, rowCount))
        }
    }
};

const AsyncList = connect(
    null,
    mapDispatchToProps
)(AsyncListPure);

export default AsyncList;