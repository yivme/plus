import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import './style.css';

class SearchPure extends Component {

    state = {
        error: ''
    };

    handleSubmit = e => {
        e.preventDefault();

        const form = e.currentTarget;
        const value = form.querySelector('[name="username"]').value;

        if (value === '') {
            this.setState({
                error: 'Это поле является обязательным для заполнения'
            });
        } else {
            this.props.goToUser(value);
        }
    };

    handleChange = () => {
        this.setState({
            error: ''
        });
    };

    render() {
        const { error } = this.state;

        return <div className='ui-search'>
            <form onSubmit={this.handleSubmit} className='ui-search__form'>
                <div>search:</div>
                <div >
                    <input
                        type='text'
                        name='username'
                        placeholder='user name'
                        onChange={this.handleChange}
                    />
                    {error && <p>{error}</p>}
                </div>
                <div>
                    <button className="ui-search__form-button" type="submit" value="Submit">
                        Показать
                    </button>
                </div>
            </form>
        </div>;
    }
}

const mapDispatchToProps = dispatch => {
    return {
        goToUser: (userName) => {
            dispatch(push(`/user/${userName}`))
        }
    }
};

const Search = connect(
    null,
    mapDispatchToProps
)(SearchPure);

export default Search;