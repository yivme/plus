import {
    GET_USER_INFO
} from '../constants/User';
import { DIRECTION, ITEM_STEP } from '../utils/constants';

const initialState = {
    direction: DIRECTION.ASC,
    rowCount: ITEM_STEP,
    userRepoList: []
};

export default function userstate(state = initialState, action) {
    switch (action.type) {
        case GET_USER_INFO:
            const { list, direction, rowCount } = action.payload;

            return { ...state, userRepoList: list, direction, rowCount };
        default:
            return state
    }
}
